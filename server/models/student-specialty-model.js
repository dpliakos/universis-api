import util from 'util';
import _ from 'lodash';
import async from 'async';
import StudentMapper from './student-mapper';
import {ValidationResult} from "../errors";
import {DataObject} from "@themost/data/data-object";
import {TraceUtils} from "@themost/common/utils";
import {EdmMapping} from "@themost/data/odata";

@EdmMapping.entityType('StudentSpecialty')
/**
 * @class
 * @property {*} specialty
 * @property {*} student
 * @augments StudentMapper
 */
class StudentSpecialty extends DataObject {
    constructor() {
        super();
    }

    validate(callback) {
        const self = this, context = self.context;
        try {
            self.studentOf(function (err, result) {
                if (err) {
                    return callback(err);
                }
                if (_.isNil(result)) {
                    return callback(new Error(context.__('Student is null or undefined')));
                }
                /**
                 *
                 * @type {Student|*}
                 */
                const student = context.model('Student').convert(result);
                student.programOf(function (err, program) {
                    const rules = context.model('Rule');
                    let validationResults;
                    const additionalTypes = ['SpecialtyRule'];
                    const q = rules.where('target').equal(program).and('additionalType').in(additionalTypes).and('programSpecialty').equal(self.specialty.specialty).prepare(true);

                    q.silent().all(function (err, rulesData) {
                        if (err) {
                            return callback(err);
                        }
                        if (rulesData.length === 0) {
                            return callback(null, [new ValidationResult(true, 'SUCC', context.__('Student semester selection does not have any rule.'))]);
                        }
                        async.eachSeries(rulesData, function (item, cb) {
                            const ruleModel = context.model(item.refersTo + 'Rule');
                            if (_.isNil(ruleModel)) {
                                return callback(new Error(context.__('Student validation rule type cannot be found.')));
                            }
                            const rule = ruleModel.convert(item);
                            rule.validate(self, function (err, result) {
                                if (err) {
                                    return callback(err);
                                }
                                /**
                                 * @type {ValidationResult[]}
                                 */
                                validationResults = validationResults || [];
                                validationResults.push(result);
                                cb();
                            });
                        }, function (err) {
                            if (err) {
                                return callback(err);
                            }

                            const fnValidateExpression = function (x) {
                                try {
                                    let expr = x['ruleExpression'];
                                    if (_.isEmpty(expr)) {
                                        return false;
                                    }
                                    expr = expr.replace(/\[%(\d+)]/g, function () {
                                        if (arguments.length === 0) return;
                                        const id = parseInt(arguments[1]);
                                        const v = validationResults.find(function (y) {
                                            return y.id === id;
                                        });
                                        if (v) {
                                            return v.success.toString();
                                        }
                                        return 'false';
                                    });
                                    expr = expr.replace(/\bAND\b/g, ' && ').replace(/\bOR\b/g, ' || ').replace(/\bNOT\b/g, ' !');
                                    return eval(expr);
                                }
                                catch (e) {
                                    return e;
                                }
                            };

                            const fnTitleExpression = function (x) {
                                try {
                                    let expr = x['ruleExpression'];
                                    if (_.isEmpty(expr)) {
                                        return false;
                                    }
                                    expr = expr.replace(/\[%(\d+)]/g, function () {
                                        if (arguments.length === 0) return;
                                        const id = parseInt(arguments[1]);
                                        const v = validationResults.find(function (y) {
                                            return y.id === id;
                                        });
                                        if (v) {
                                            return '(' + v.message.toString() + ')';
                                        }
                                        return 'Unknown Rule';
                                    });
                                    expr = expr.replace(/\bAND\b/g, context.__(' AND ')).replace(/\bOR\b/g, context.__(' OR ')).replace(/\bNOT\b/g, context.__(' NOT '));
                                    return expr;
                                }
                                catch (e) {
                                    return e;
                                }
                            };

                            const finalValidationResults = [];
                            additionalTypes.forEach(function (t) {
                                let res, title;
                                //filter by rule type
                                const filtered = rulesData.filter(function (x) {
                                    return x['additionalType'] === t;
                                });
                                if (filtered.length > 0) {
                                    //apply default expression
                                    const expr = filtered.find(function (x) {
                                        return !_.isEmpty(x['ruleExpression']);
                                    });
                                    if (expr) {
                                        res = fnValidateExpression(expr);
                                        title = fnTitleExpression(expr);
                                    }
                                    else {
                                        //get expression (for this rule type)
                                        const ruleExp1 = filtered.map(function (x) {
                                            return '[%' + x.id + ']';
                                        }).join(' AND ');
                                        res = fnValidateExpression({ ruleExpression: ruleExp1 });
                                        title = fnTitleExpression({ ruleExpression: ruleExp1 });
                                    }
                                    //build
                                    let finalResult = new ValidationResult(res, res === true ? 'SUCC' : 'FAIL', title);
                                    finalResult.type = t;
                                    const childValidationResults = [];
                                    filtered.forEach(function (x) {
                                        childValidationResults.push.apply(childValidationResults, validationResults.filter(function (y) {
                                            return x.id === y.id;
                                        }));
                                    });
                                    if (childValidationResults.length === 1) {
                                        if (finalResult.success === childValidationResults[0].success) {
                                            finalResult = childValidationResults[0];
                                        }
                                        finalResult.type = t;
                                    }
                                    else {
                                        finalResult.validationResults = childValidationResults;
                                    }
                                    finalValidationResults.push(finalResult);
                                }

                            });
                            if (finalValidationResults.length === 0) {
                                return callback(null, new ValidationResult(true , 'SUCC'));
                            }
                            else {
                                const success = finalValidationResults.filter( x=> {
                                    return x.success === false;
                                }).length === 0;
                                return callback(null, Object.assign(new ValidationResult(success , success ? 'SUCC': 'FAIL'), {
                                    validationResults: finalValidationResults
                                }));
                            }
                        });
                    });
                });
            });
        }
        catch (err) {
            callback(err);
        }
    }

    /**
     *
     * @param {DataContext=} context
     * @returns {Promise<ValidationResult|*>}
     */
    async saveAsync(context) {
        const self = this;
        // ensure context
        context = context || this.context;
            // validate state and catch error
            try {
                await new Promise((resolve, reject) => {
                    return self.validateState(function (err) {
                        if (err) {
                            return reject(err);
                        }
                        return resolve();
                    });
                })
            }
            catch (err) {
                // trace error
                TraceUtils.error(err);
                this.validationResult = new ValidationResult(false, err.code || 'EFAIL', context.__('Cannot change student specialty.'), err.message);
                return this.validationResult;
            }
            // validate and catch error
            let validationResult;
            try {
                validationResult = await new Promise((resolve, reject) => {
                    // call validate
                    return self.validate(function (err, result) {
                        if (err) {
                            return reject(err);
                        }
                        return resolve(result);
                    });
                });
                this.validationResult = validationResult;
                if (validationResult && validationResult.success === false) {
                    return this.validationResult;
                }
            }
            catch (err) {
                // check error and return validation result
                if (err instanceof ValidationResult) {
                    this.validationResult = err;
                    return this.validationResult;
                } else {
                    // trace error
                    TraceUtils.error(err);
                    // return validation result
                    this.validationResult = new ValidationResult(false, err.code || 'FAIL', self.context.__('Cannot change student specialty.'), err.message);
                    return this.validationResult;
                }
            }
            // get student extra data
            const student = await context.model('Student')
                .where('id').equal(this.student)
                .select('id', 'studentIdentifier', 'person/name as name', 'specialty')
                .silent()
                .getItem();
            // save student
            await context.model('Student').silent().save({
                id: this.student.id,
                specialtyId: this.specialty.specialty
            });
            // save event log item
            await context.model('EventLog').silent().save({
                title: `Αλλαγή κατεύθυνσης φοιτητή [${student.studentIdentifier}] ${student.name} 
από την κατεύθυνση <${student.specialty}> στην κατεύθυνση <${this.specialty.name}>`,
                eventType: 2,
                username: (context.interactiveUser && context.interactiveUser.name) || (context.user && context.user.name)
            });
            return this.validationResult;
        }

    save(context, callback) {
        if (typeof callback === 'function') {
            return this.saveAsync(context || this.context).then(() => {
                return callback();
            }).catch(err => {
                return callback(err);
            });
        }
        return this.saveAsync(context || this.context)
    }

    validateState(done) {
        const self = this, context = self.context;
        //get programSpecialty object
        self.programSpecialty(function (err, specialty) {
            if (err) {
                return done(err);
            }
            self.studentOf(function (err, result) {
                if (err) {
                    return done(err);
                }
                if (_.isNil(result)) {
                    return done(new Error('Student is null or undefined'));
                }
                /**
                 *
                 * @type {Student|*}
                 */
                const student = context.model('Student').convert(result);
                if (_.isNil(self.student))
                    self.student=result;

                //check student status
                return  student.canSelectSpecialty().then(value => {
                    if (value.success) {
                        //check if new specialty exists in available specialties
                        return student.availableProgramSpecialties(function (err, specialties) {
                            if (err) {
                                return done(err);
                            }
                            const currentSpecialty = specialties.filter(function (x) {
                                return (x.id === specialty.id);
                            });
                            if (_.isNil(currentSpecialty)) {
                                return done(new Error(context.__('Program specialty cannot be found.')));
                            }
                            return done();
                        });
                    }
                    else {
                        return done(value);
                    }
                }).catch( err=> {
                    return done(err);
                });
            });
        });
    }

    programSpecialty(callback) {
        const self = this;
        let specialtyId = self.specialty;
        const context = self.context;
        if (_.isObject(self.specialty)) {
            specialtyId = self.specialty.id;
        }
        context.model("StudyProgramSpecialty").where("id").equal(specialtyId).flatten().silent().first(function (err, specialty) {
            if (err) {
                return callback(err);
            }
            if (_.isNil(specialty)) {
                return callback(new Error(context.__('Program specialty cannot be found.')));
            }
            self.specialty=specialty;
            callback(null, specialty);
        });
    }
}

StudentSpecialty.prototype.studentOf = StudentMapper.prototype.studentOf;

module.exports = StudentSpecialty;
