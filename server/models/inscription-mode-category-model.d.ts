import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';
/**
 * @class
 */
declare class InscriptionModeCategory extends DataObject {

     
     /**
      * @description Id
      */
     public id: number; 
     
     /**
      * @description The name of the item.
      */
     public name: string; 

}

export = InscriptionModeCategory;