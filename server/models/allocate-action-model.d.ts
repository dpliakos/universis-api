import {EdmMapping,EdmType} from '@themost/data/odata';
import OrganizeAction = require('./organize-action-model');

/**
 * @class
 */
declare class AllocateAction extends OrganizeAction {

     
     /**
      * @description Μοναδικός κωδικός
      */
     public id: number; 

}

export = AllocateAction;