import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import Department = require('./department-model');
import AcademicYear = require('./academic-year-model');
import ScholarshipType = require('./scholarship-type-model');
import ScholarshipStatus = require('./scholarship-status-model');

/**
 * @class
 */
declare class Scholarship extends DataObject {

     
     /**
      * @description Ο μοναδικός κωδικός της υποτροφίας
      */
     public id: number; 
     
     /**
      * @description Το τμήμα που προσφέρει την υποτροφία.
      */
     public department: Department|any; 
     
     /**
      * @description Η ονομασία της υποτροφίας
      */
     public name?: string; 
     
     /**
      * @description Ο οργανισμός που προσφέρει την υποτροφία
      */
     public organization?: string; 
     
     /**
      * @description Το όνομα του υπεύθυνου επικοινωνίας.
      */
     public contactName?: string; 
     
     /**
      * @description Το ακαδημαϊκό έτος που προσφέρεται η υποτροφία.
      */
     public year?: AcademicYear|any; 
     
     /**
      * @description Ο μέγιστος αριθμός των φοιτητών που μπορεί να λάβει την υποτροφία.
      */
     public totalStudents?: number; 
     
     /**
      * @description Σημειώσεις
      */
     public notes?: string; 
     
     /**
      * @description Τύπος υποτροφίας
      */
     public scholarshipType?: ScholarshipType|any; 
     
     /**
      * @description Κατάσταση υποτροφίας
      */
     public status?: ScholarshipStatus|any; 
     
     /**
      * @description Η ημερομηνία τροποποίησης του στοιχείου
      */
     public dateModified: Date; 

}

export = Scholarship;