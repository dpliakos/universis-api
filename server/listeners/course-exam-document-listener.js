import util from 'util';
import {LangUtils,TraceUtils} from '@themost/common/utils';
import _ from 'lodash';

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
   try {
       if (event.state !== 2) return callback();
       const context = event.model.context;
       /**
        * @type {CourseExamDocument|DataObject}
        */
       const target = event.target;
       const status = target.documentStatus;
       context.model('CourseExamDocument').where("id").equal(target.id).flatten().first(function (err, result) {
           if (err) {
               callback(err);
           }
           else {
               const prevStatus =result["documentStatus"] || 0;
               event.previous = result;
               if (status===3 && prevStatus===1)
               {
                   event.target.documentStatusReason="Ακύρωση αποστολής από το χρήστη."
               }
               callback();
           }
       });
   }
   catch (e) {
       callback(e)
   }
}

/**
 * @param {DataEventArgs} event
 * @param {function(Error=)} callback
 */
export function afterSave(event, callback) {
    try {
        if (event.state !== 2) return callback();
        const context = event.model.context, target = event.target;

        const status = target.documentStatus;
        let eventTitle = '';
        const prevStatus = event.previous["documentStatus"] || 0;
        //add to event but first get course exam doc from db
        const courseExamDocumentModel = context.model('CourseExamDocument');
        if (LangUtils.parseInt(status) === 3 && prevStatus === 1) {
            //document is cancelled
            courseExamDocumentModel.where('id').equal(target.id).expand('courseExam').silent().first(function (err, doc) {
                if (err) {
                    return callback(err);
                }
                if (!_.isNil(doc)) {

                    courseExamDocumentModel.resolveMethod('me', [], function (err, user) {
                        if (err) {
                            return callback(err);
                        }
                        const query = util.format("sp_cancelCourseExamGradesDocument %s,%s", target.id, user);
                        context.db.execute(query, null, function (err, result) {
                            if (err) {
                                TraceUtils.error(err);
                                return callback(err);
                            }
                            else {
                                const docId = result[0].docId;
                                if (docId!==target.id)
                                {
                                    TraceUtils.error(`Cancelling document [${target.id} - ${doc.courseExam}]  is not allowed due to its state or you do not have permissions [${docId}]`);
                                    return callback(new Error(context.__('Cancelling document is not allowed due to its state or you do not have permissions.')));
                                }
                                context.unattended(function (cb) {
                                    eventTitle = util.format('Ακύρωση βαθμολογίου [%s] της εξεταστικής περιόδου [%s] %s %s-%s %s.'
                                        , doc.id, doc.courseExam.id, doc.courseExam.name, doc.courseExam.year, doc.courseExam.year + 1, doc.courseExam.examPeriod);
                                    context.model('EventLog').save({
                                        title: eventTitle,
                                        eventType: 2,
                                        username: context.interactiveUser.name,
                                        eventSource: 'teachers',
                                        eventApplication: 'universis'
                                    }, function (err) {
                                        cb(err);
                                    });
                                }, function (err) {
                                    if (err) {
                                        TraceUtils.error(err);
                                    }
                                    callback(err, docId);
                                });
                            }
                        });
                    });
                }
                else {
                    callback();
                }
            });
        }
        else {
            if (typeof status !== 'undefined' && LangUtils.parseInt(status) !== LangUtils.parseInt(prevStatus)) {
                return callback(new Error(context.__('You cannot change document status due to its state.')));
            }
            else {
                callback();
            }
        }
    }
    catch (e) {
        callback(e);
    }
}