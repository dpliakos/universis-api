import {AttachmentFileSystemStorage} from '@themost/web/files'
import path from 'path';
/**
 * @class
 */
export class PrivateContentService extends AttachmentFileSystemStorage {
    constructor() {
        super(path.resolve("content/private"));
        this.virtualPath = '/api/content/private/';
    }
}
